package ch.usi.dslab.lel.dynastarv2.probject;

/*
 * ScalableSMR - A library for developing Scalable services based on SMR
 * Copyright (C) 2017, University of Lugano
 *
 *  This file is part of ScalableSMR.
 *
 *  ScalableSMR is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

import java.io.Serializable;

/**
 * Created by longle on 17.07.17.
 */

public class ObjId implements Serializable, Comparable<ObjId> {
    private static final long serialVersionUID = 1L;

    public int value;

    public ObjId() {
    }

    public ObjId(ObjId other) {
        this.value = other.value;
    }

    public ObjId(int value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(this.value);
    }

    @Override
    public boolean equals(Object other_) {
        try {
            ObjId other = ((ObjId) other_);
            return this.value == other.value;
        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public String toString() {
        return "(" + value + ")";
    }


    public String getStringValue() {
        return String.valueOf(this.value);
    }

    @Override
    public int compareTo(ObjId other) {
        return Integer.compare(this.value, other.value);
    }

}

#!/usr/bin/python

import inspect
import os
import sys


def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

#
if len(sys.argv) != 2:
    print "usage: " + sys.argv[0] + " <number of clients>"
    sys.exit(1)

# client_id = sys.argv[1] + " "

client_count = int(sys.argv[1]);
java_bin = "java -XX:+UseG1GC -Dlog4j.configuration=file:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/dynastar/target/classes/log4j.xml -cp "
app_classpath = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/dynastar/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/netwrapper/target/classes:/Users/longle/.m2/repository/org/apache/logging/log4j/log4j-api/2.0-rc1/log4j-api-2.0-rc1.jar:/Users/longle/.m2/repository/org/apache/logging/log4j/log4j-core/2.0-rc1/log4j-core-2.0-rc1.jar:/Users/longle/.m2/repository/net/jpountz/lz4/lz4/1.2.0/lz4-1.2.0.jar:/Users/longle/.m2/repository/io/dropwizard/metrics/metrics-core/3.1.2/metrics-core-3.1.2.jar:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/libmcad/target/classes:/Users/longle/.m2/repository/multicast/spread/4.4.0/spread-4.4.0.jar:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/URingPaxos/target/classes:/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/lib/*:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/dynastar/target/dependencies/* "
gathererCommand = "java -cp " + app_classpath + " ch.usi.dslab.bezerra.sense.DataGatherer 60000 /Users/longle/tmp/dynastar latency client_latency " + str(
    client_count) + " throughput client_tp " + str(client_count)
print(gathererCommand)


# client_cmd = java_bin + app_classpath + client_class
os.system(gathererCommand)

import inspect
import json
import logging
import os
import shlex
import subprocess
import sys
import threading

__author__ = 'longle'

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')


class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            logging.debug('Thread started')
            run_args = shlex.split(self.cmd)
            self.process = subprocess.Popen(run_args)
            self.process.communicate()
            logging.debug('Thread finished')

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            logging.debug('Terminating process')
            self.process.terminate()
            thread.join()
        return self.process.returncode


class LauncherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        for cmd in self.cmdList:
            logging.debug("Executing: %s", cmd["cmdstring"])
            sshcmdbg(cmd["node"], cmd["cmdstring"])


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


def sshcmd(node, cmdstring, timeout=None):
    finalstring = "ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\""
    logging.debug(finalstring)
    cmd = Command(finalstring)
    return cmd.run(timeout)


def localcmd(cmdstring, timeout=None):
    logging.debug("localcmd:%s", cmdstring)
    cmd = Command(cmdstring)
    return cmd.run(timeout)


def sshcmdbg(node, cmdstring):
    cmd = "ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\" &"
    logging.debug("sshcmdbg: %s", cmd)
    os.system(cmd)


def localcmdbg(cmdstring):
    logging.debug("localcmdbg: %s", cmdstring)
    os.system(cmdstring + " &")


def get_system_config_file(config_type):
    if config_type is None:
        return {'partitioning': SYSTEM_CONFIG_DIR + '/partitioning.json',
                'system_config': SYSTEM_CONFIG_DIR + '/system_config.json'}
    partitioning_file = SYSTEM_CONFIG_DIR + '/' + config_type + '_partitioning.json'
    system_config_file = SYSTEM_CONFIG_DIR + '/' + config_type + '_system_config.json'
    if not os.path.isfile(partitioning_file):
        logging.error('ERROR: parititoning file not found: %s', partitioning_file)
        sys.exit(1)
    if not os.path.isfile(system_config_file):
        logging.error('ERROR: system config file not found: %s', system_config_file)
        sys.exit(1)
    return {'partitioning': partitioning_file,
            'system_config': system_config_file}


def read_json_file(file_name):
    file_stream = open(file_name)
    content = json.load(file_stream)
    file_stream.close()
    return content


def sarg(i):
    return sys.argv[i]


def iarg(i):
    return int(sarg(i))


def farg(i):
    return float(sarg(i))


# parameters
GLOBAL_HOME = os.path.normpath(script_dir() + '/../../')

BIN_HOME = os.path.normpath(GLOBAL_HOME + '/dynastarv2/bin')

DYNASTAR_HOME = os.path.normpath(GLOBAL_HOME + '/dynastarv2')
DYNASTAR_CP = os.path.normpath(DYNASTAR_HOME + '/target/classes')
DYNASTAR_CLASS_SERVER = os.path.normpath('ch.usi.dslab.lel.dynastarv2.sample.AppServer')
DYNASTAR_CLASS_ORACLE = os.path.normpath('ch.usi.dslab.lel.dynastarv2.sample.AppOracle')
DYNASTAR_CLASS_CLIENT = os.path.normpath('ch.usi.dslab.lel.dynastarv2.sample.AppClient')

LIBMCAD_HOME = os.path.normpath(GLOBAL_HOME + '/libmcad')
LIBMCAD_CP = os.path.normpath(LIBMCAD_HOME + '/target/classes')
LIBMCAD_CLASS_RIDGE = 'ch.usi.dslab.bezerra.mcad.ridge.RidgeEnsembleNode'

NETWRAPPER_HOME = os.path.normpath(GLOBAL_HOME + '/netwrapper')
NETWRAPPER_CP = os.path.normpath(NETWRAPPER_HOME + '/target/classes')

RIDGE_HOME = os.path.normpath(GLOBAL_HOME + '/ridge')
RIDGE_CP = os.path.normpath(RIDGE_HOME + '/target/classes')

SENSE_HOME = os.path.normpath(GLOBAL_HOME + '/sense')
SENSE_CP = os.path.normpath(SENSE_HOME + '/target/classes')

SYSTEM_CONFIG_DIR = os.path.normpath(BIN_HOME + '/systemConfigs')

LIBMCAST_HOME = os.path.normpath(GLOBAL_HOME + '/libjmcast')
LIBMCAST_CP = os.path.normpath(LIBMCAST_HOME + '/target/classes')
LIBMCAST_PAXOS_PROCESS = os.path.normpath(LIBMCAST_HOME + '/libmcast/build/local/bin/proposer-acceptor')


DEPENDENCIES_DIR = os.path.normpath(GLOBAL_HOME + '/dependencies/*')

_class_path = [os.path.normpath(GLOBAL_HOME + '/dependencies/guava-19.0.jar'), DYNASTAR_CP, LIBMCAST_CP, LIBMCAD_CP,
               NETWRAPPER_CP, RIDGE_CP, SENSE_CP, DEPENDENCIES_DIR]

JAVA_BIN = 'java -XX:+UseG1GC -Xmx8g -Dlog4j.configuration=file:' + script_dir() + '/log4j.xml'
JAVA_CLASSPATH = '-cp \'' + ':'.join([str(val) for val in _class_path]) +"\'"

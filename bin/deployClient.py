#!/usr/bin/python

import inspect
import logging
import os
import sys

import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


if len(sys.argv) != 5:
    print "usage: " + sys.argv[0] + " <clientId> <numPermits> <interactive> <config_mode>"
    #./deployClient.py 1 1 true minimal
    sys.exit(1)

client_id = common.iarg(1)
numPermits = common.iarg(2)
interactive = common.sarg(3)
mode = common.sarg(4)
partitioning_file = common.get_system_config_file(mode)['partitioning']
system_config_file = common.get_system_config_file(mode)['system_config']

logging.info('Starting client: %s', str(sys.argv))

client_cmd = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.DYNASTAR_CLASS_CLIENT,
              client_id, system_config_file, partitioning_file, numPermits, interactive, ]
client_cmd = " ".join([str(val) for val in client_cmd])

logging.info(client_cmd)
os.system(client_cmd)

package ch.usi.dslab.lel.dynastarv2;
/*
 * ScalableSMR - A library for developing Scalable services based on SMR
 * Copyright (C) 2017, University of Lugano
 *
 *  This file is part of ScalableSMR.
 *
 *  ScalableSMR is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.dynastarv2.command.Command;
import ch.usi.dslab.lel.dynastarv2.messages.CommandType;
import ch.usi.dslab.lel.dynastarv2.messages.DynaStarMessageType;
import ch.usi.dslab.lel.dynastarv2.probject.ObjId;
import ch.usi.dslab.lel.dynastarv2.probject.PRObject;
import ch.usi.dslab.lel.dynastarv2.probject.PRObjectNode;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by longle on 17.07.17.
 */
public abstract class PartitionStateMachine extends StateMachine {

    private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private AtomicInteger changesCount = new AtomicInteger(0);
    private int FEEDBACK_INTERVAL = 1000;
    private AtomicInteger nextCommandId = new AtomicInteger(0);
    public PartitionStateMachine(int replicaId, String systemConfig, String partitionsConfig) {
        super(replicaId, systemConfig, partitionsConfig, null);
        this.logger = LoggerFactory.getLogger(PartitionStateMachine.class);
        MDC.put("ROLE", replica.getPartition().getType() + "-" + replica.getPartitionId() + "/" + replicaId);
        this.setLogger(logger);
        logger.info("Available processors: {}", Runtime.getRuntime().availableProcessors());
    }

    @Override
    public void runStateMachine() {
        readySemaphore.release();
        while (replica.running) {
            logger.debug("taking command from queue, queue size {}", replica.executionQueue.size());
            Command command = getNextCommandToExecute();
            logger.debug("cmd {} is ready to execute", command.getId());

            // TIMELINE STUFF
            command.t_partition_dequeued = System.currentTimeMillis();

            boolean done = false;

            Object attempt = attemptToExecuteGatherCommand(command);
            if (attempt instanceof CompletableFuture) {
                logger.debug("cmd {} is an async one, going to execute it, current active threads {}", command.getId(), ((ThreadPoolExecutor) executorService).getActiveCount());
                // command is a gather command
                // and this is source replica, wait for getting back obj
                ((CompletableFuture) attempt).thenAccept(o -> logger.debug("cmd {} finish async execution", command.getId())).exceptionally(o -> {
                    ((Exception) o).printStackTrace();
                    return null;
                }).exceptionally(e -> {
                    ((Exception) e).printStackTrace();
                    return 0;
                });
                continue;
            }
            if (attempt instanceof Boolean && (boolean) attempt) {
                // command is a gather command
                // and this is destination replica, don't do any thing
                continue;
            }

            attempt = attemptToExecuteRepartitionCommand(command);
            if (attempt instanceof Boolean && (boolean) attempt) {
                continue;
            }

            //cmd is not a gather command
            done = checkForNodesAvailability(command, "RETRY");
            if (done) continue;

            logger.debug("cmd {} processing app command {}", command.getId(), command);

            int count = changesCount.addAndGet(updateObjectGraphAsClientCommand(command));
            logger.debug("cmd {} change count {}", command.getId(), count);
            if (count > FEEDBACK_INTERVAL && this.objectGraph.getUpdatedEdgesBatch().size() > 0) {
                sendFeedback();
                changesCount.addAndGet(-FEEDBACK_INTERVAL);
            }

            attempt = attemptToExecuteCommand(command);
            if (attempt instanceof Boolean) continue;

        }
    }

    private boolean attemptToExecuteRepartitionCommand(Command command) {
        if (!command.isRepartitioning()) return false;
        int partitioningVersion = (int) command.getItem(0);
        if (partitioningVersion < this.objectGraph.getPartitioningVersion())
            return true; // receive old partitioning map
        logger.debug("cmd {} ready to run repartitioning #{}", command.getId(), partitioningVersion);
        Map<Integer, Set<ObjId>> processingMap = this.objectGraph.calculateRepartitioningMoves(partitioningVersion);
        for (int destPartitionId : processingMap.keySet()) {
            if (destPartitionId != this.partitionId) {
                Set<ObjId> objIds = processingMap.get(destPartitionId);
                Map<ObjId, Message> diffs = new HashMap<>();
                objIds.forEach(objId -> {
                    PRObjectNode node = this.objectGraph.getNode(objId);
                    if (node != null) {
                        diffs.put(objId, new Message(node.deepClone()));
//                        diffs.put(objId, new Message(node));
                        if (node.getPRObject() == null) {
                            logger.error("cmd {} REPARTITIONING source, object content is null {}, full cmd {}", command.getId(), node, command.toFullString());
                            System.out.println("ERROR: cmd " + command.getId());
                        } else {
                            logger.trace("cmd {} REPARTITIONING source, move object {} to partition {}", command.getId(), node, destPartitionId);
                        }
                    } else {
                        logger.error("REPARTITIONING source, can't find object {} in this partition for command {}. Request retry", objId, command.getId());
                        System.out.println("REPARTITIONING source, can't find object " + objId + " in this partition for command " + command.getId() + ". Request retry");
                        System.exit(1);
                    }
                });
                ArrayList<Partition> partitions = new ArrayList<>();
                partitions.add(Partition.getPartition(destPartitionId));
                Message exchange = new Message(DynaStarMessageType.PROBJECT_BUNDLE_REPARTITIONING, command.getId(), diffs, getPartitionId());
                logger.debug("cmd {} REPARTITIONING sending {} objects", command.getId(), diffs.keySet().size());
                sendObject(partitions, command, exchange);
                removeObjects(command, objIds);
            }
        }
        Set<ObjId> expectedOjbIds = processingMap.get(this.partitionId);
        logger.debug("cmd {} REPARTITIONING source, start waiting for objects {} to be returned", command.getId(), expectedOjbIds.size());
        Set<PRObjectNode> objs = replica.waitForObjectExchanging(command, processingMap);
        logger.debug("cmd {} REPARTITIONING source, received enough objects  ", command.getId());
        logger.info("applying new partitioning #{}", partitioningVersion);
        this.objectGraph.applyNewPartitioning(partitioningVersion);
        return true;
    }

    private void sendFeedback() {
        logger.debug("sending {} feedback to oracle", this.objectGraph.getUpdatedEdgesBatch().size());
        Message exchange = new Message(DynaStarMessageType.PARTITION_FEEDBACK, this.partitionId, this.objectGraph.getUpdatedEdgesBatch());
        ArrayList<Partition> partitions = new ArrayList<>(Partition.getOracleSet());
        replica.reliableMulticast(partitions, exchange);
        this.objectGraph.clearUpdatedBatch();
    }

    private int updateObjectGraphAsClientCommand(Command command) {
        Set<ObjId> objIds = Utils.extractObjectId(command);
        logger.debug("cmd {} updating edges between objects", command.getId(), objIds);
        return this.objectGraph.connect(objIds);
    }

    private Map<Integer, Set<ObjId>> gatherObjects(Command command, int destPartId) {
        Set<ObjId> objIds = Utils.extractObjectId(command);
        logger.debug("cmd {} gather objects: {} to partition {}", command.getId(), objIds, destPartId);
        int moveCount = 0;
        Map<Integer, Set<ObjId>> srcMap = new HashMap<>();
        Set<Partition> multicastDestPart = new HashSet<>();


        for (ObjId objId : objIds) {
            PRObjectNode objLoc = this.objectGraph.getNode(objId);
            logger.trace("cmd {} analyzing objects: {} belongs to partition {}", command.getId(), objId, objLoc.getPartitionId());

            if (objLoc.getPartitionId() != destPartId) { // only moveObject objects not in same partition
                int srcPartId = objLoc.getPartitionId();
                if (srcMap.get(srcPartId) == null) srcMap.put(srcPartId, new HashSet<>());
                srcMap.get(srcPartId).add(objId);
                multicastDestPart.add(Partition.getPartition(srcPartId));
                moveCount++;
            }
        }
        if (moveCount == 0) { //only oracle has this case
            logger.debug("cmd {} no move needed, forward to partition destPartId", command.getId());
            command.setObjectMap(srcMap);
            forwardCommand(command, destPartId, false);
        } else {
            logger.debug("cmd {} perform {} moves to partition {}", command.getId(), moveCount, destPartId);
            multicastDestPart.add(Partition.getPartition(destPartId));

            Command moveCmd = new Command(destPartId, srcMap, command);
            moveCmd.setId(command.getId());
            moveCmd.setDestinations(multicastDestPart);
            moveCmd.setObjectMap(srcMap);
            moveCmd.setPartitioningVersion(this.objectGraph.getPartitioningVersion());

            //Set target partition for original command as the destination partition. This is for picking partition to reply
            Set tmp = new HashSet<Partition>();
            tmp.add(Partition.getPartition(destPartId));
            command.setDestinations(tmp);
            command.setPartitioningVersion(this.objectGraph.getPartitioningVersion());
            if (moveTPMonitor != null) moveTPMonitor.incrementCount();
            ClientMessage wrapper = new ClientMessage(DynaStarMessageType.GATHER, moveCmd);

            if (replica.isPartitionMulticaster(command)) {
                logger.debug("cmd {} Partition {} multicast move command {} to partitions ", command.getId(), getPartitionId(), moveCmd, multicastDestPart);
                replica.multicast(multicastDestPart, wrapper);
            }
        }
        return srcMap;
    }

    private Object attemptToExecuteCommand(Command command) {
        Set<ObjId> objIds = Utils.extractObjectId(command);
        if (!shouldMove(command)) {
            logger.debug("cmd {} going to execute locally", command.getId());
            boolean done = attemptToExecuteGenericCommand(command);
            if (done) return done;
            command.rewind();
            Message reply = executeCommand(command);
            replica.sendReply(reply, command);
            return new Boolean(true);
        }
        logger.debug("cmd {} CANNOT execute locally, trying to gather objects", command.getId());
        Map<Integer, Set<ObjId>> gatherObjectMap = gatherObjects(command, getPartitionWithMostObjects(objIds));
        return new Boolean(true);
    }

    private boolean attemptToExecuteGenericCommand(Command command) {
        if (!isLocalCommand(command)) return false;
        command.rewind();
        CommandType cmdType = (CommandType) command.getNext();
        logger.debug("cmd {} processing local {} command", command.getId(), cmdType);
        Message reply = null;
        switch (cmdType) {
            case CREATE: {
                Set<PRObject> objs = (Set<PRObject>) command.getNext();
                Set<PRObjectNode> created = new HashSet<>();
//                Object value = command.getNext();
//                PRObject obj = this.createObject(objId, value);
                objs.forEach(prObject -> {
                    PRObject newObj = createObject(prObject);
                    logger.debug("cmd {} created object {}", command.getId(), newObj);
                    PRObjectNode node = new PRObjectNode(newObj, newObj.getId(), getPartitionId());
                    this.objectGraph.addNode(node);
                    created.add(node);
                });
                reply = new Message(created);
                break;
            }
            case READ: {
                ObjId objId = (ObjId) command.getNext();
                PRObject obj = this.objectGraph.getPRObject(objId);
                reply = new Message(obj);
                break;
            }
            case DELETE: {
                ObjId objId = (ObjId) command.getNext();
                this.objectGraph.removeNode(objId);
                reply = new Message("OK");
                break;
            }
        }
        if (reply != null) {
            replica.sendReply(reply, command);
        }
        return true;
    }

    protected abstract PRObject createObject(PRObject prObject);

    public abstract Message executeCommand(Command command);


    public void updateObjectGraphAsOracleMap(Map<Integer, Set<ObjId>> map) {
        logger.debug("updating objectGraph as oracle map...{}", map);
        this.objectGraph.updateGraph(map);
    }


    public Object attemptToExecuteGatherCommand(Command commandWrapper) {
        if (!commandWrapper.isGathering()) return false;
        int destPartId = (int) commandWrapper.getNext();
        Map<Integer, Set<ObjId>> srcPartMap = (HashMap) commandWrapper.getNext();
        Command command = (Command) commandWrapper.getNext();
//        logger.debug("cmd {} gather command: send to {} as map {}", command.getId(), destPartId, srcPartMap);
        if (getPartitionId() == destPartId) { // if this is the destination partition
            //lock not only object being exchanged, but also object of actual command
            lockObjects(Utils.extractObjectId(command), command.getId());
            logger.debug("cmd {} destination start waiting for objects", command.getId());

            CompletableFuture<Message> task = CompletableFuture.supplyAsync(() -> {
                logger.debug("cmd {} waiting for receiving object", command.getId());
                Set<PRObjectNode> objs = replica.waitForObjectLending(command, srcPartMap);
                boolean done = attemptToExecuteGenericCommand(command);
                if (!done) {
                    command.rewind();
                    Message reply = executeCommand(command);
                    replica.sendReply(reply, command);
                }
                returnObjects(command, srcPartMap);
                unlockObjects(command.getId());
                return null;
            }, executorService);
            return task;
        } else {
            Set<ObjId> objIds = srcPartMap.get(getPartitionId());
            logger.debug("cmd {} source, trying to send objects {} from this partition {} to partition {}", command.getId(), objIds, getPartitionId(), destPartId);
            Map<ObjId, Message> diffs = new HashMap<>();
            objIds.forEach(objId -> {
                PRObjectNode node = this.objectGraph.getNode(objId);
                if (node != null) {
                    diffs.put(objId, new Message(node.deepClone()));
//                    diffs.put(objId, new Message(node));
                    if (node.getPRObject() == null) {
                        logger.error("cmd {} source, object content is null {}, full cmd {}", command.getId(), node, command.toFullString());
                        System.out.println("ERROR: cmd " + command.getId());
                    } else {
                        logger.trace("cmd {} source, move object {} to partition {}", command.getId(), node, destPartId);
                    }
                } else {
                    logger.error("source, can't find object {} in this partition for command {}. Request retry", objId, command.getId());
                    System.out.println("source, can't find object " + objId + " in this partition for command " + command.getId() + ". Request retry");
                    System.exit(1);
                }
            });
            ArrayList<Partition> partitions = new ArrayList<>();
            partitions.add(Partition.getPartition(destPartId));
            Message exchange = new Message(DynaStarMessageType.PROBJECT_BUNDLE_LENDING, command.getId(), diffs, getPartitionId());

            //lock not only object being exchanged, but also object of actual command
//            lockObjects(Utils.extractObjectId(command), command.getId());
            lockObjects(objIds, command.getId());
            sendObject(partitions, command, exchange);
            removeObjects(command, objIds);
            CompletableFuture taskWaitingForReceivingBackObject = CompletableFuture.supplyAsync(() -> {
                logger.debug("cmd {} source, start waiting for objects {} to be returned", command.getId(), objIds);
                Set<PRObjectNode> objs = replica.waitForObjectReturning(command, objIds, destPartId);
                logger.debug("cmd {} source, received enough objects in {}", command.getId(), objIds);
                objs.forEach(prObjectNode -> {
                    if (this.objectGraph.getNode(prObjectNode.getId()).getPRObject() == null) {
                        System.out.println("WTF?? NULLLL " + prObjectNode + " - " + this.objectGraph.getNode(prObjectNode.getId()));
                    }
                });
                unlockObjects(command.getId());
                return null;
            }, executorService).exceptionally(e -> {
                logger.error(e.getMessage());
                e.printStackTrace();
                return null;
            });
            return taskWaitingForReceivingBackObject;
        }
    }

    private void sendObject(List<Partition> destination, Command command, Message objects) {
        if (!replica.isPartitionMulticaster(command))
            return;
        replica.reliableMulticast(destination, objects);
    }

    private void returnObjects(Command command, Map<Integer, Set<ObjId>> gatherObjectMap) {
        if (!replica.isPartitionMulticaster(command))
            return;
        logger.debug("cmd {} returning objects {}", command.getId(), gatherObjectMap);
        gatherObjectMap.keySet().stream().filter(partitionId -> partitionId != getPartitionId()).forEach(partitionId -> {
            Set<ObjId> objIds = gatherObjectMap.get(partitionId);
            Map<ObjId, Message> diffs = new HashMap<>();
            objIds.forEach(objId -> {
                PRObjectNode node = this.objectGraph.getNode(objId);
                if (node != null && node.getPRObject() != null) {
                    diffs.put(objId, new Message(node));
//                    logger.debug("cmd {} source, return object {} to partition {}", command.getId(), node.getPRObject(), partitionId);
                } else if (node != null) {
                    logger.error("cmd {} source, return object {} to partition {} but got null", command.getId(), node, partitionId);
                    System.out.println("return but get null" + command.getId() + " - " + node);
                } else {
                    logger.error("source, can't find object {} in this partition for command {}", objId, command.getId());
                    System.out.println("source, can't find object " + objId + " in this partition for command " + command.getId() + ". Request retry");
                    System.exit(1);
                }
            });
            Message exchange = new Message(DynaStarMessageType.PROBJECT_BUNDLE_RETURNING, command.getId(), diffs, getPartitionId());
            ArrayList<Partition> partitions = new ArrayList<>();
            partitions.add(Partition.getPartition(partitionId));
            replica.reliableMulticast(partitions, exchange);
            removeObjects(command, objIds);
        });
    }


    public PRObject getObject(ObjId id) {
        return this.objectGraph.getPRObject(id);
    }

    public void indexObject(PRObject obj, int partitionId) {
        PRObjectNode node = new PRObjectNode(obj, obj.getId(), partitionId);
        this.objectGraph.addNode(node);
    }

    public void removeObject(ObjId id) {
        this.objectGraph.removeObject(id);
    }

    public void removeObjects(Command cmd, Set<ObjId> objIds) {
        logger.debug("cmd {} remove objects {}", cmd.getId(), objIds);
        objIds.forEach(objId -> removeObject(objId));
    }

    public void handlePartitionMap(Message m) {
        m.rewind();
        DynaStarMessageType type = (DynaStarMessageType) m.getNext();
        Map<Integer, Set<ObjId>> map = (Map<Integer, Set<ObjId>>) m.getNext();
        int partitioningId = (int) m.getNext();
        logger.debug("REPARTITIONING - saving new partitionning map #{}...", partitioningId);
        this.objectGraph.savePartitionMap(map, partitioningId);
        Message signal = new Message(DynaStarMessageType.PARTITION_MAP_RECEIVED, this.partitionId, partitioningId);
        ArrayList<Partition> partitions = new ArrayList<>(Partition.getOracleSet());
        replica.reliableMulticast(partitions, signal);
        logger.debug("REPARTITIONING - inform the oracle {} with message {} ", partitions, signal);
    }

    @Override
    public void handlePartitionCommand(Command command) {
        logger.debug("handling partitioning command {}", command);
        replica.queueNextAwaitingExecution(command);
    }
}

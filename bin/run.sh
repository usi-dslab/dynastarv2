#!/usr/bin/env bash
tmux send-keys -t 0 "clear && tmux clear-history && ./deployMcast.py minimal" Enter
tmux send-keys -t 1 "clear && tmux clear-history && ./deployOracle.py minimal |& tee oracle.out" Enter
tmux send-keys -t 2 "clear && tmux clear-history && ./deployServer.py -1 minimal |& tee server.out" Enter
tmux send-keys -t 3 "clear && tmux clear-history && ./deployClient.py 1 1 true minimal|& tee client.out" Enter
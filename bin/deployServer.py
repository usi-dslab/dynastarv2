#!/usr/bin/python
import logging
import os
import sys

import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

if len(sys.argv) != 3:
    print "usage: " + sys.argv[0] + " <serverId:number/-1:all> <config_mode>"
    #./deployServer.py -1 minimal -> start all server

    sys.exit(1)

mode = common.sarg(2)
server_to_start = common.iarg(1)

partitioning_file = common.get_system_config_file(mode)['partitioning']
system_config_file = common.get_system_config_file(mode)['system_config']
paritioning = common.read_json_file(partitioning_file)
system_config = common.read_json_file(system_config_file)


def get_host_type(_host):
    for _group in paritioning["partitions"]:
        if _host["pid"] in _group["servers"]:
            return _group["type"]


if server_to_start == -1:

    logging.info('Starting all servers...')
    cmdList = []
    for member in system_config["group_members"]:
        pid = member["pid"]
        group = member["group"]
        host = member["host"]
        port = member["port"]
        if get_host_type(member) == "PARTITION":
            launchNodeCmdString = [common.JAVA_BIN, common.JAVA_CLASSPATH,
                                   common.DYNASTAR_CLASS_SERVER,
                                   pid, system_config_file, partitioning_file]
            launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])
            cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

    logging.info(cmdList)
    thread = common.LauncherThread(cmdList)
    thread.start()
    thread.join()

else:
    logging.info('Starting server %d', server_to_start)
    launchNodeCmdString = [common.JAVA_BIN, common.JAVA_CLASSPATH,
                           common.DYNASTAR_CLASS_SERVER,
                           paritioning['partitions'][server_to_start]['servers'][0],
                           # paritioning['partitions'][server_to_start]['id'],
                           system_config_file, partitioning_file]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])
    logging.info(launchNodeCmdString)
    os.system(launchNodeCmdString)

package ch.usi.dslab.lel.dynastarv2.partitioning;
/*
 * ScalableSMR - A library for developing Scalable services based on SMR
 * Copyright (C) 2017, University of Lugano
 *
 *  This file is part of ScalableSMR.
 *
 *  ScalableSMR is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

import ch.usi.dslab.lel.dynastarv2.OracleStateMachine;
import ch.usi.dslab.lel.dynastarv2.Partition;
import ch.usi.dslab.lel.dynastarv2.command.CmdId;
import ch.usi.dslab.lel.dynastarv2.command.Command;
import ch.usi.dslab.lel.dynastarv2.messages.DynaStarMessageType;
import ch.usi.dslab.lel.dynastarv2.probject.ObjId;
import ch.usi.dslab.lel.dynastarv2.probject.PRObjectGraph;
import ch.usi.dslab.lel.dynastarv2.probject.PRObjectNode;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Stream;

/**
 * Created by longle on 25.08.17.
 */
public class MetisPartitioner implements Partitioner {

    private static final Logger logger = LoggerFactory.getLogger(OracleStateMachine.class);
    private OracleStateMachine oracleStateMachine;
    private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private Map<Integer, Map<Integer, Set<ObjId>>> METISPartitionMaps = new HashMap<>();
    private Map<Integer, Set<Integer>> signalsMap = new HashMap<>();

    public MetisPartitioner(OracleStateMachine oracleStateMachine) {
        this.oracleStateMachine = oracleStateMachine;
    }

    @Override
    public void repartition(PRObjectGraph objectGraph, int partitionsCount) {
        Future<Map<Integer, Set<ObjId>>> task = executorService.submit(new PartitioningWorker(objectGraph, partitionsCount));
        int partitioningId = oracleStateMachine.objectGraph.getPartitioningVersion() + 1;
        try {
            Map<Integer, Set<ObjId>> ret = task.get();
            if (ret == null) {
                logger.info("Partition unsuccessful #{}", partitioningId);
                return;
            }
            logger.info("Partitioning successfully #{}", partitioningId);
            objectGraph.savePartitionMap(ret, partitioningId);
            CmdId cmdid = new CmdId(this.oracleStateMachine.getReplicaId(), this.oracleStateMachine.getNextCommandId());
            Command partitionCommand = new Command(DynaStarMessageType.ORACLE_PARTITION_MAP, ret, partitioningId);
            partitionCommand.setId(cmdid);
            Set<Partition> dests = Partition.getPartitionList();
            partitionCommand.setDestinations(dests);
            logger.debug("going to send this message size {} to partitions {}, map size", partitionCommand.getPackSize(), dests, ret.size());
            this.oracleStateMachine.getReplica().reliableMulticast(new ArrayList<>(dests), partitionCommand);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void queueSignal(int partitionId, int partitioningId) {
        Set<Integer> signals = signalsMap.get(partitioningId);
        if (signals == null) {
            signals = new HashSet<>();
            signalsMap.put(partitioningId, signals);
        }
        signals.add(partitionId);
    }

    @Override
    public boolean isSignalsFullfilled(int partitioningId) {
        Set<Integer> signals = signalsMap.get(partitioningId);
        Assert.assertNotNull(signals);
        return signals.size() == Partition.getPartitionsCount();
    }


    class PartitioningWorker implements Callable<Map<Integer, Set<ObjId>>> {
        int partitionsCount;
        PRObjectGraph objectGraph;

        public PartitioningWorker(PRObjectGraph objectGraph, int partitionsCount) {
            this.partitionsCount = partitionsCount;
            this.objectGraph = objectGraph;
        }

        private String generateMetisInput(PRObjectGraph objectGraph) {
            List<String> lines = new ArrayList<>();
            lines.add(objectGraph.getEdges().size() + " " + objectGraph.getNodesCount());
            for (PRObjectGraph.Edge edge : objectGraph.getEdges()) {
                StringBuffer line = new StringBuffer();
                for (PRObjectNode node : edge.nodes) {
                    line.append((node.getId().value + 1) + " ");
                }
                lines.add(line.toString());
            }
            String metisInputFileName = "hmetis.inp." + oracleStateMachine.getReplicaId() + "." + System.nanoTime();
            Path file = Paths.get(metisInputFileName);
            try {
                Files.write(file, lines, Charset.forName("UTF-8"));
                return metisInputFileName;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private boolean processWithMetis(String fileName) {
            boolean success = false;
            try {
                // start up the command in child process
//                ProcessBuilder ps = new ProcessBuilder("/usr/local/bin/shmetis", fileName, String.valueOf(this.partitionsCount), "10");
                ProcessBuilder ps = new ProcessBuilder("hmetis", fileName, String.valueOf(this.partitionsCount));

                ps.redirectErrorStream(true);

                Process pr = ps.start();

                BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    if (line.indexOf("******") > -1) success = true;
                    logger.info(line);
                }
                pr.waitFor();
                in.close();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return success;
        }

        private Map<Integer, Set<ObjId>> parseMetisPartition(String fileName) {
            Map<Integer, Set<ObjId>> tmp = new HashMap<>();
            Partition.getPartitionList().forEach(partition -> tmp.put(partition.getId(), new HashSet<>()));
            try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
                int[] metisUserId = {0};
                stream.forEachOrdered(s -> {
                    ObjId objId = new ObjId(metisUserId[0]);
                    tmp.get(Integer.parseInt(s) + 1).add(objId);
                    metisUserId[0]++;
                });
//                boolean result = Files.deleteIfExists(Paths.get(fileName)); //surround it in try catch block
            } catch (IOException e) {
                e.printStackTrace();
            }
            return tmp;
        }

        @Override
        public Map<Integer, Set<ObjId>> call() throws Exception {
            String fileName = this.generateMetisInput(objectGraph);
            this.processWithMetis(fileName);
            Map<Integer, Set<ObjId>> ret = this.parseMetisPartition(fileName + ".part." + this.partitionsCount);
            return ret;
        }
    }
}

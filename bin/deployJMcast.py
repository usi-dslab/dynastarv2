#!/usr/bin/python

import sys
import logging

import common

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

if len(sys.argv) !=2:
    print "usage: " + sys.argv[0] + " config_mode:string "
    # ./deployMcast minial
    sys.exit(1)

mode = common.sarg(1)
system_config_file = common.get_system_config_file(mode)['system_config']
config = common.read_json_file(system_config_file)

ensembleCmdLists = []
lastEnsemble = -1
cmdList = []


paxos_groups = config["paxos_groups"]
paxos_config_file = config["paxos_config_file"]

cmdList = []
count=0
for group in paxos_groups:
    for node in group["group_members"]:
        launchNodeCmdPieces = [common.LIBMCAST_PAXOS_PROCESS, node["pid"],
                               paxos_config_file + '.' + str(group["group_id"]),
                               "> /tmp/px"+str(count)+".log"]
        count+=1
        launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
        cmdList.append({"node": node['host'], "cmdstring": launchNodeCmdString})
server_thread = common.LauncherThread(cmdList)
server_thread.start()
server_thread.join()

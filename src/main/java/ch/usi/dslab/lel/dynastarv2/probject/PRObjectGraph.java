package ch.usi.dslab.lel.dynastarv2.probject;
/*
 * ScalableSMR - A library for developing Scalable services based on SMR
 * Copyright (C) 2017, University of Lugano
 *
 *  This file is part of ScalableSMR.
 *
 *  ScalableSMR is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

import ch.usi.dslab.lel.dynastarv2.PartitionStateMachine;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by longle on 17.07.17.
 */
public class PRObjectGraph {
    private static PRObjectGraph instance = null;
    Logger logger = LoggerFactory.getLogger(PartitionStateMachine.class);
    private Map<ObjId, PRObjectNode> objectMapping = null;
    private Set<Edge> edges = null;
    private Set<Edge> updatedEdgesBatch = null;
    private int partitioningVersion = 0;
    //    private Map<Integer, Set<ObjId>> nextPartitionMap;
    private int partitionId;
    private Map<Integer, Map<Integer, Set<ObjId>>> partitioningMap;
    private Set<ObjId> localObjects;

    protected PRObjectGraph(int partitionId) {
        this.objectMapping = new ConcurrentHashMap<>();
        this.edges = new HashSet<>();
        this.updatedEdgesBatch = new HashSet<>();
        this.partitionId = partitionId;
        this.partitioningMap = new HashMap<>();
    }

    public static PRObjectGraph getInstance(int partitionId) {
        if (instance == null) {
            instance = new PRObjectGraph(partitionId);
        }
        return instance;
    }

    public PRObject getPRObject(ObjId objId) {
        PRObjectNode node = objectMapping.get(objId);
        if (node != null && node.getPRObject() != null) return node.getPRObject();
        return null;
    }

    public PRObjectNode getNode(ObjId objId) {
        return objectMapping.get(objId);
    }

    public void addNode(PRObjectNode prObject) {
        objectMapping.put(prObject.getId(), prObject);
    }

    public void removeNode(PRObjectNode prObject) {
        removeNode(prObject.getId());
    }

    public void removeNode(ObjId objId) {
        objectMapping.remove(objId);
    }

    public int getPartitioningVersion() {
        return partitioningVersion;
    }

    public void setPartitioningVersion(int partitioningVersion) {
        this.partitioningVersion = partitioningVersion;
    }

    public void updatePRObject(ObjId objId, PRObjectNode node) {
        objectMapping.put(objId, node);
    }

    public void invalidate() {
        objectMapping = new ConcurrentHashMap<>();
    }

    private int getOptimizedDestinationWithMinimumMove(Map<ObjId, Integer> objectMap) {
        Map<Integer, Integer> calculation = new HashMap<>();
        Set<ObjId> objIds = objectMap.keySet();
        objIds.forEach(objId -> {
            int partitionId = getNode(objId).getPartitionId();
            if (calculation.get(partitionId) == null) calculation.put(partitionId, 1);
            else calculation.put(partitionId, calculation.get(partitionId) + 1);
        });
        int dest = -1;
        int score = -1;
        for (int partitionId : calculation.keySet()) {
            if (calculation.get(partitionId) > score) {
                score = calculation.get(partitionId);
                dest = partitionId;
            }
        }
        return dest;
    }

    public Set<ObjId> getMissingNodes(Set<ObjId> objIds) {
        Set<ObjId> ret = new HashSet<>();
        for (ObjId objId : objIds) {
            if (objId == null) {
                continue;
            }
            if (getNode(objId) == null)
                ret.add(objId);
        }
        return ret;
    }

    public void updateGraph(Map<Integer, Set<ObjId>> map) {
        map.keySet().forEach(partitionId -> {
            Set<ObjId> objIds = map.get(partitionId);
            objIds.forEach(objId -> addOrUpdateNode(objId, partitionId));
        });
    }

    private PRObjectNode addOrUpdateNode(ObjId objId, Integer partitionId) {
        PRObjectNode node = getNode(objId);
        if (node == null) {
            logger.debug("obj {} doesn't exist in memory, going to create one for partition {}", objId, partitionId);
            node = new PRObjectNode(objId, partitionId);
            addNode(node);
        }
        node.setPartitionId(partitionId);
        return node;
    }


    public int connect(Set<ObjId> objIds) {
        int count = 0;
        Set<PRObjectNode> nodes = objIds.stream().map(objId -> getNode(objId)).collect(Collectors.toSet());
        Edge e = new Edge(nodes);
        if (!edges.contains(e)) {
            edges.add(e);
            updatedEdgesBatch.add(e);
            count++;
        }
        return count;
    }

    public Set<Edge> getUpdatedEdgesBatch() {
        return this.updatedEdgesBatch;
    }

    public void clearUpdatedBatch() {
        updatedEdgesBatch.clear();
    }

    public void persitObject(PRObjectNode obj) {
        PRObjectNode node = getNode(obj.getId());
        if (node == null) {
            // only when object is being moved from another partition in repartitioning process
            // thus that object would belong to this partition after all;
            node = addOrUpdateNode(obj.getId(), this.partitionId);
        }
        if (obj.getPRObject() == null) {
            logger.error("object {} have null content", obj.getId());
        }
        node.setPRObject(obj.getPRObject());
    }

    public void removeObject(ObjId id) {
        PRObjectNode node = getNode(id);
        Assert.assertNotNull(node);
        node.setPRObject(null);
    }

    public int updateGraphEdges(Set<Edge> newEdges) {
        int count = 0;
        for (Edge edge : newEdges) {
            if (!this.edges.contains(edge)) {
                this.edges.add(edge);
                count++;
            }
        }
        return count;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public int getNodesCount() {
        return this.objectMapping.keySet().size();
    }

    public void savePartitionMap(Map<Integer, Set<ObjId>> map, int partitioningVersion) {
        this.partitioningMap.put(partitioningVersion, map);
    }

    public void applyNewPartitioning(int partitioningVersion) {
        this.updateGraph(this.partitioningMap.get(partitioningVersion));
        this.partitioningVersion = partitioningVersion;
    }

    public Map<Integer, Set<ObjId>> calculateRepartitioningMoves(int partitioningVersion) {
        Map<Integer, Set<ObjId>> ret = new HashMap<>();
        Map<Integer, Set<ObjId>> nextPartitionMap = this.partitioningMap.get(partitioningVersion);
        for (int targetPartitionId : nextPartitionMap.keySet()) {
            Set<ObjId> target = nextPartitionMap.get(targetPartitionId);
            Set<ObjId> objToProcess = new HashSet<>();
            ret.put(targetPartitionId, objToProcess);
            for (ObjId objId : target) {
                if (this.partitionId == targetPartitionId) {
                    if (this.getPRObject(objId) == null) {
                        objToProcess.add(objId);
                    }
                    continue;
                }
                if (this.getPRObject(objId) != null) {
                    objToProcess.add(objId);
                }
            }
        }
        return ret;
    }

    public Set<ObjId> getLocalObjects() {
        Set<ObjId> ret = new HashSet<>();
        for (ObjId objId : this.objectMapping.keySet()) {
            if (this.objectMapping.get(objId).getPartitionId() == this.partitionId) ret.add(objId);
        }
        return ret;

    }

    public static class Edge {
        public Set<PRObjectNode> nodes = new HashSet<>();

        public Edge() {

        }

        public Edge(Set<PRObjectNode> nodes) {
            this.nodes = nodes;
        }

        public void addNode(PRObjectNode node) {
            this.nodes.add(node);
        }

        public void addNode(Set<PRObjectNode> nodes) {
            this.nodes.addAll(nodes);
        }

        @Override
        public boolean equals(Object obj) {
            return nodes.equals(((Edge) obj).nodes);
        }

        @Override
        public int hashCode() {
            return nodes.hashCode();
        }

        @Override
        public String toString() {
            return "[Edges-" + this.nodes.size() + "]";
        }
    }
}
